class Recipes {
  String label;
  String imageUrl;
  static List<Recipes> sample = [
    Recipes(
      'Spaghetti and Meatballs',
      'assets/ccc612ad-5f65-4ef8-9912-dde0b581ebd8-hairy-bikers-spaghetti-and-meatballs-recipe.jpg',
      4,
      [
        Ingredient(
          1,
          'Ingredients: 1 lb. spaghetti\n1 lb. ground beef\n1/3 c. bread crumbs\n1/4 c. finely chopped parsley\n1/4 c. freshly grated Parmesan, plus more for serving\n1 large egg\n2 garlic cloves, minced\nKosher salt\n1/2 tsp. red pepper flakes\n2 tbsp. extra-virgin olive oil\n1/2 c. onion, finely chopped\n1 (28-oz.) can crushed tomatoes\n1 bay leaf\nFreshly ground black pepper',
          '.\nDirections:\n1. In a large pot of salted boiling water, cook pasta until al dente. Drain\n2.In a large bowl, combine beef with bread crumbs, parsley, Parmesan, egg, garlic, 1 teaspoon salt, and red pepper flakes. Mix until just combined then form into 16 balls.\n3.In a large pot over medium heat, heat oil. Add meatballs and cook, turning occasionally, until browned on all sides, about 10 minutes. Transfer meatballs to a plate.\n4. Add onion to pot and cook until soft, 5 minutes. Add crushed tomatoes and bay leaf. Season with salt and pepper and bring to a simmer. Return meatballs to pot and cover. Simmer until sauce has thickened, 8 to 10 minutes.\n5. Serve pasta with a healthy scoop of meatballs and sauce. Top with Parmesan before serving.   ',
        ),
      ],
    ),
    Recipes(
      'Tomato Soup',
      'assets/27729023535_a57606c1be.jpg',
      5,
      [
        Ingredient(
          1,
          'Ingredients: \n1 tablespoon unsalted butter or margarine\n1 tablespoon olive oil\n1 onion, thinly sliced\n2 large garlic cloves, peeled and crushed\n2 (28 ounce) cans whole peeled tomatoes\n1cup of water\n1 tablespoon sugar\n1 teaspoon salt, plus more to taste\nfreshly ground black pepper to taste\n1 pinch red pepper flakes\n¼ teaspoon celery seed',
          '.\nDirection:\n1. Heat butter and olive oil in a large saucepan over medium-low heat and cook onion and garlic until onion is soft and translucent, about 5 minutes. Add tomatoes, water, sugar, salt, pepper, red pepper flakes, celery seed, and oregano. Bring to a boil. Reduce heat, cover, and simmer for 15 minutes.\n2.Remove from heat and puree with an immersion blender. Reheat soup until warm and season with more salt and pepper if desired. ',
        ),
      ],
    ),
    Recipes(
      'Grilled Cheese',
      'assets/3187380632_5056654a19_b.jpg',
      6,
      [
        Ingredient(
          1,
          'Ingridients:\nprefered slices of white bread\nbutter\nprefered slices of any type of cheese\nsalt',
          '.\nDirections:\n1. Spread butter on each slice of bread\n2.place your cheese in between the sclices and close the sandwich together\n3.preheat a pan with cooking oil\n4.place your sandwich on the pan and grill it.\nyou can also place it on your oven to be roasted there.\nstart serving your sandwich',
        ),
      ],
    ),
    Recipes(
      'Chocolate Chip Cookies',
      'assets/15992102771_b92f4cc00a_b.jpg',
      7,
      [
        Ingredient(
          1,
          'Ingredients:\n 1/4cups Gold Medal™ all-purpose flour\n1teaspoon baking soda\n1/2teaspoon salt\n1cup butter, softened\n3/4cup granulated sugar\n3/4cup packed brown sugar\n1egg\n1teaspoon vanilla\n2cups semisweet chocolate chips\n1cup coarsely chopped nuts, if desired\n',
          '.\nDirections:\n1.Heat oven to 375°F. In small bowl, mix flour, baking soda and salt; set aside.\n2.In large bowl, beat softened butter and sugars with electric mixer on medium speed, or mix with spoon about 1 minute or until fluffy, scraping side of bowl occasionally.\n3.Beat in egg and vanilla until smooth. Stir in flour mixture just until blended (dough will be stiff). Stir in chocolate chips and nuts.\n4.Onto ungreased cookie sheets, drop dough by rounded tablespoonfuls 2 inches apart\n5.Bake 8 to 10 minutes or until light brown (centers will be soft). Cool 2 minutes; remove from cookie sheet to cooling rack. Cool completely, about 30 minutes. Store covered in airtight container.',
        ),
      ],
    ),
    Recipes(
      'Taco Salad',
      'assets/8533381643_a31a99e8a6_c.jpg',
      8,
      [
        Ingredient(
          1,
          'Ingredients:\nGround beef\nAvocado oil\nTaco seasoning\nRomaine lettuce\nGrape tomato\nShredded cheese\nAvocado\nOnions\nSalsa\nSour cream ',
          '.\nDirections:\n1.Heat oil in a skillet over high heat. Add ground beef and cook until browned.\n2. Stir seasoning into the ground beef until combined. If you like, you can also add 1/4 cup of water when adding the seasoning and let it simmer a bit\n3.Place the lettuce, tomatoes, cheese, avocado, onions, salsa, and sour cream into a large bowl. When the taco salad meat is done, add it to the bowl.\n4.Serve in bowls or plate individually.',
        ),
      ],
    ),
    Recipes(
      'Hawaiian Pizza',
      'assets/15452035777_294cefced5_c.jpg',
      9,
      [
        Ingredient(
          1,
          'Ingredients:\n1/2 recipe homemade pizza crust\n1/2 cup (127g) pizza sauce (homemade or store-bought)\n1 and 1/2 cups (6oz or 168g) shredded mozzarella cheese\n1/2 cup (75g) cooked ham or Canadian bacon, sliced or chopped\n1/2 cup (82g) pineapple chunks (canned or fresh)\n3 slices bacon, cooked and crumbled',
          '.\nDirections:\n1.Prepare pizza dough through step 5, including preheating the oven to 475°F (246°C). Cover the shaped dough lightly with plastic wrap and allow it to rest as the oven preheats\n2.To prevent the pizza toppings from making your pizza crust soggy,\n brush the shaped dough lightly with olive oil. Using your fingers, push dents into the surface of the dough to prevent bubbling.\n Top the dough evenly with pizza sauce, then add the cheese, ham, pineapple, and bacon\nBake pizza for 12-15 minutes. Remove from the oven and top with fresh basil, if desired. Slice hot pizza and serve immediatel',
        ),
      ],
    ),
  ];

  Recipes(
    this.label,
    this.imageUrl,
    this.servings,
    this.ingredients,
  );

  int servings;
  List<Ingredient> ingredients;
}

class Ingredient {
  double quantity;
  String measure;
  String name;

  Ingredient(this.quantity, this.measure, this.name);
}
