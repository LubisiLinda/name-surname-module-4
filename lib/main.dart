import 'package:flutter/material.dart';
import 'screens/animated_splashscreen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Splash Screen',
      theme: ThemeData(
        primarySwatch: Colors.orange,
      ),
      home: TheSplashScreen(),
      debugShowCheckedModeBanner: false,
    );
  }
}
